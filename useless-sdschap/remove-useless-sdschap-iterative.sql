/*
 * Remove Empty sdschap.
 * 1- get the frozen and validated sds_doc.
 * 2- get all sdschapters for those frozen sdsdocs.
 * 3- get empty sdschapters for them.
 *    - we considred that empty chapter is a leaf chapter and doesn't connected with neither phractxt nor phraftxt.
 * 4- exclude empty chapters that related minimalist. 
 * 5- remove remained empty chapters.
 * 
 * Note: as the tree structure of chapters(with depth 1->7), we can run this script many times to remove empty chapters from the buttom(leafs) up to root level by level.
 * */
DROP TABLE IF EXISTS useless_sdschap;
CREATE TEMPORARY TABLE IF NOT EXISTS useless_sdschap
(
    sdschap_id bigint
);

WITH
/* on live DB. */
    frozen_and_validated AS
        (SELECT sdsdoc_id,
                max(tpstill) AS deletion_dt
         FROM sdsdoc
         WHERE (status = 'E-ARCHIVED' OR cr_dt < '1-1-2020')
           AND tp IN ('SDS') -- ('TEMPLATE', 'PIF', 'TEMPLATE_COSMETIC_LABEL', 'PCK_LABEL', 'SKU_LABEL', 'FDSpeyraud', '', NULL)
         GROUP BY sdsdoc_id
         HAVING max(tpstill) <= '9999-12-31'),
    /* get full chapters list for those sdsdoc.*/
    chapters_for_frozen_documents AS
        (SELECT ventr_src_id AS sdsdoc_id,
                ventr_tp     AS linktype,
                sdschap.*,
                ventr_num1   AS parentSdschap_id,
                ventr_num2   AS deep,
                ventr_float2 AS inheritedDeep
         FROM fventr_sdsdoc_sdschap_subsdschap((SELECT array_agg(sdsdoc_id) FROM frozen_and_validated))
                  INNER JOIN sdschap ON sdschap_id = ventr_dst_id
         UNION ALL
         SELECT entr_src_id                                                                               AS sdsdoc_id,
                entr_tp                                                                                   AS linktype,
                sdschap.*,
                entr_src_id                                                                               AS parentSdschap_id,
                0                                                                                         AS deep,
                CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep
         FROM entr_sdsdoc_sdschap
                  INNER JOIN sdschap ON sdschap_id = entr_dst_id
         WHERE entr_src_id IN (SELECT sdsdoc_id FROM frozen_and_validated)),
/* -get empty chapter from the previous list. 
 * - as our consideration: empty sdschap is leaf chapter with no links to both phractxt and phraftxt.*/
    empty_chapters AS
        (SELECT leaf_chap.sdsdoc_id,
                leaf_chap.sdschap_id AS sdschap_id,
                leaf_chap.numero
         FROM
             -- get leaf chapters.
             (SELECT sch.sdsdoc_id,
                     sch.sdschap_id,
                     sch.numero
              FROM chapters_for_frozen_documents sch
                       LEFT JOIN entr_sdschap_sdschap ess ON
                  ess.entr_src_id = sch.sdschap_id
              WHERE ess.entr_id IS NULL) leaf_chap

                 -- get phractxt and phraftxt
                 -- in this query phraftxt and phractxt is undependent from each other.
                 LEFT JOIN entr_sdschap_phractxtke espc ON
                 leaf_chap.sdschap_id = espc.entr_src_id AND espc.entr_tp = 'SELECTED'

                 LEFT JOIN entr_sdschap_phraftxt espf ON
                 leaf_chap.sdschap_id = espf.entr_src_id AND espf.entr_tp = 'SELECTED'
         WHERE espc.entr_dst_id IS NULL
           AND espf.entr_dst_id IS NULL),
/* get the chapters of minimalist sdsdoc. */
    minimalist AS
        (SELECT ventr_src_id AS sdsdoc_id,
                ventr_tp     AS linktype,
                sdschap.*,
                ventr_num1   AS parentSdschap_id,
                ventr_num2   AS deep,
                ventr_float2 AS inheritedDeep
         FROM fventr_sdsdoc_sdschap_subsdschap(ARRAY [2])
                  INNER JOIN sdschap ON sdschap_id = ventr_dst_id
         UNION ALL
         SELECT entr_src_id                                                                               AS sdsdoc_id,
                entr_tp                                                                                   AS linktype,
                sdschap.*,
                entr_src_id                                                                               AS parentSdschap_id,
                0                                                                                         AS deep,
                CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep
         FROM entr_sdsdoc_sdschap
                  INNER JOIN sdschap ON sdschap_id = entr_dst_id
         WHERE entr_src_id IN (2))
/* get empty chapters except that related to minimalist sdsdoc. */
INSERT
INTO useless_sdschap
SELECT empty_chapters.sdschap_id
FROM empty_chapters
         LEFT JOIN minimalist USING (numero)
WHERE empty_chapters.sdschap_id NOT IN (SELECT sdschap_id FROM useless_sdschap)
  AND minimalist.numero IS NULL;


------------------------------------------------------------------------------------------------Extract Useless Sdschap-----------------------------------------------
/* extract useless sdschap. */
SELECT *
FROM useless_sdschap
         INNER JOIN sdschap s USING (sdschap_id);


/* extract useless-sdschap relations: */
-- der_sdschap:
SELECT der_sdschap.*
FROM useless_sdschap
         INNER JOIN der_sdschap ON der_src_id = sdschap_id;

-- entr_sdsdoc_sdschap:
SELECT entr_sdsdoc_sdschap.*
FROM useless_sdschap
         INNER JOIN entr_sdsdoc_sdschap ON entr_dst_id = sdschap_id;

-- entr_sdschap_sdselt:
SELECT entr_sdschap_sdselt.*
FROM useless_sdschap
         INNER JOIN entr_sdschap_sdselt ON entr_src_id = sdschap_id;

-- entr_sdschap_sdschap
SELECT entr_sdschap_sdschap.*
FROM useless_sdschap
         INNER JOIN entr_sdschap_sdschap ON entr_src_id = sdschap_id OR entr_dst_id = sdschap_id;

-- entr_sdschap_phractxtke:
/* here we maight get relations because we assume that empty chapters are chapters have no relations with phractxtke where tp is 'selected' */
SELECT entr_sdschap_phractxtke.*
FROM useless_sdschap
         INNER JOIN entr_sdschap_phractxtke ON entr_src_id = sdschap_id;

-- entr_sdschap_phraftxt:
SELECT entr_sdschap_phraftxt.*
FROM useless_sdschap
         INNER JOIN entr_sdschap_phraftxt ON entr_src_id = sdschap_id;

--------------------------------------------------------------------------------Delete Useless Sdschap---------------------------------------------------------------
/* delete useless sdschap */

-- entr_sdsdoc_sdschap:
DELETE
FROM entr_sdsdoc_sdschap USING useless_sdschap
WHERE entr_dst_id = sdschap_id;

-- entr_sdschap_sdschap
DELETE
FROM entr_sdschap_sdschap USING useless_sdschap
WHERE entr_src_id = sdschap_id
   OR entr_dst_id = sdschap_id;

-- sdschap:
DELETE
FROM sdschap USING useless_sdschap
WHERE sdschap.sdschap_id = useless_sdschap.sdschap_id;
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
######  ######  #######
#     # #     #    #    #####   ####
#     # #     #    #    #    # #
######  #     #    #    #    #  ####
#     # #     #    #    #####       #
#     # #     #    #    #      #    #
######  ######     #    #       ####
*/

SELECT dblink_connect('xmat-tps',
                      'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
DROP TABLE useless_sdschap;
CREATE TEMPORARY TABLE IF NOT EXISTS useless_sdschap
    (sdschap_id bigint);

/* get frozen and deleted sdsdoc before 9999-12-29 */
WITH frozen_and_validated AS (SELECT sdsdoc_id,
    max(tpstill) AS deletion_dt
FROM sdsdoc
WHERE (status = 'E-ARCHIVED'
    OR cr_dt < '1-1-2020')
    AND tp IN ('SDS') -- not in ('TEMPLATE', 'PIF', 'TEMPLATE_COSMETIC_LABEL', 'PCK_LABEL', 'SKU_LABEL', 'FDSpeyraud', '', NULL)
GROUP BY sdsdoc_id
HAVING max(tpstill) <= '9999-12-31'),
    /* get full chapters list for those sdsdoc.*/
    chapters_for_frozen_documents AS
            (SELECT ventr_src_id AS sdsdoc_id,
            ventr_tp AS linktype,
            sdschap.*,
            ventr_num1 AS parentSdschap_id,
            ventr_num2 AS deep,
            ventr_float2 AS inheritedDeep
        FROM fventr_sdsdoc_sdschap_subsdschap((SELECT array_agg(sdsdoc_id) FROM frozen_and_validated))
            INNER JOIN sdschap ON sdschap_id = ventr_dst_id
        UNION ALL
        SELECT entr_src_id AS sdsdoc_id,
            entr_tp AS linktype,
            sdschap.*,
            entr_src_id AS parentSdschap_id,
            0 AS deep,
            CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep
        FROM entr_sdsdoc_sdschap
            INNER JOIN sdschap ON sdschap_id = entr_dst_id
        WHERE entr_src_id IN (SELECT sdsdoc_id FROM frozen_and_validated)),
    /* -get empty chapter from the previous list.
     * - as our consideration: empty sdschap is leaf chapter with no links to both phractxt and phraftxt.*/
    empty_chapters AS
            (SELECT leaf_chap.sdsdoc_id,
            leaf_chap.sdschap_id AS sdschap_id,
            leaf_chap.numero
        FROM
            -- get leaf chapters.
                (SELECT sch.sdsdoc_id,
                sch.sdschap_id,
                sch.numero
            FROM chapters_for_frozen_documents sch
                LEFT JOIN entr_sdschap_sdschap ess ON
                ess.entr_src_id = sch.sdschap_id
            WHERE ess.entr_id IS NULL
            ORDER BY sch.numero) leaf_chap

                -- get phractxt and phraftxt
                -- in this query phraftxt and phractxt is undependent from each other.
                LEFT JOIN entr_sdschap_phractxtke espc ON
                leaf_chap.sdschap_id = espc.entr_src_id AND espc.entr_tp = 'SELECTED'

                LEFT JOIN entr_sdschap_phraftxt espf ON
                leaf_chap.sdschap_id = espf.entr_src_id AND espf.entr_tp = 'SELECTED'
        WHERE espc.entr_dst_id IS NULL
            AND espf.entr_dst_id IS NULL),
    /* get the chapters of minimalist sdsdoc. */
    minimalist AS
            (SELECT ventr_src_id AS sdsdoc_id,
            ventr_tp AS linktype,
            sdschap.*,
            ventr_num1 AS parentSdschap_id,
            ventr_num2 AS deep,
            ventr_float2 AS inheritedDeep
        FROM fventr_sdsdoc_sdschap_subsdschap(ARRAY [2])
            INNER JOIN sdschap ON sdschap_id = ventr_dst_id
        UNION ALL
        SELECT entr_src_id AS sdsdoc_id,
            entr_tp AS linktype,
            sdschap.*,
            entr_src_id AS parentSdschap_id,
            0 AS deep,
            CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep
        FROM entr_sdsdoc_sdschap
            INNER JOIN sdschap ON sdschap_id = entr_dst_id
        WHERE entr_src_id IN (2))

    /* get empty chapters except that related to minimalist sdsdoc. */
INSERT INTO useless_sdschap SELECT empty_chapters.sdschap_id
FROM empty_chapters
    LEFT JOIN minimalist USING (numero)
WHERE empty_chapters.sdschap_id NOT IN (SELECT sdschap_id FROM useless_sdschap)
    AND minimalist.numero IS NULL;


------------------------------------------------------------------------------------------------Extract Useless Sdschap-----------------------------------------------
/* extract useless sdschap. */
SELECT * FROM useless_sdschap INNER JOIN sdschap s USING (sdschap_id);


/* extract useless-sdschap relations: */
-- der_sdschap:
SELECT der_sdschap.* FROM useless_sdschap INNER JOIN der_sdschap ON der_src_id = sdschap_id;

-- entr_sdsdoc_sdschap:
SELECT entr_sdsdoc_sdschap.* FROM useless_sdschap INNER JOIN entr_sdsdoc_sdschap ON entr_dst_id = sdschap_id;

-- entr_sdschap_sdselt:
SELECT entr_sdschap_sdselt.* FROM useless_sdschap INNER JOIN entr_sdschap_sdselt ON entr_src_id = sdschap_id;

-- entr_sdschap_sdschap
SELECT entr_sdschap_sdschap.* FROM useless_sdschap INNER JOIN entr_sdschap_sdschap ON entr_src_id = sdschap_id OR entr_dst_id = sdschap_id;

-- entr_sdschap_phractxtke:
/* here we maight get relations because we assume that empty chapters are chapters have no relations with phractxtke where tp is 'selected' */
SELECT entr_sdschap_phractxtke.* FROM useless_sdschap INNER JOIN entr_sdschap_phractxtke ON entr_src_id = sdschap_id;

-- entr_sdschap_phraftxt:
SELECT entr_sdschap_phraftxt.* FROM useless_sdschap INNER JOIN entr_sdschap_phraftxt ON entr_src_id = sdschap_id;

--------------------------------------------------------------------------------Delete Useless Sdschap---------------------------------------------------------------
/* delete useless sdschap */

-- der_sdschap
DELETE FROM der_sdschap USING useless_sdschap WHERE der_src_id = sdschap_id;

-- entr_sdsdoc_sdschap:
DELETE FROM entr_sdsdoc_sdschap USING useless_sdschap WHERE entr_dst_id = sdschap_id;

-- sdschap:
DELETE FROM sdschap USING useless_sdschap WHERE sdschap.sdschap_id = useless_sdschap.sdschap_id;

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');