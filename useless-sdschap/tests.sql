SELECT DISTINCT status
FROM sdsdoc


WITH frozen_and_validated AS
         (SELECT sdsdoc_id,
                 max(tpstill) AS deletion_dt
          FROM sdsdoc
          WHERE (status = 'E-ARCHIVED'
             OR cr_dt < '1-1-2020') and tp in ('SDS', '', NULL) --tp not in ('TEMPLATE', 'PIF', 'TEMPLATE_COSMETIC_LABEL')
          GROUP BY sdsdoc_id
          HAVING max(tpstill) <= '9999-12-31')
SELECT *
FROM frozen_and_validated;

