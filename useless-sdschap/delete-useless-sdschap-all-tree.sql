/*
 * Remove Empty sdschap.
 * 1- get the frozen and validated sds_doc.
 * 2- get all sdschapters for those frozen sdsdocs.
 * 3- get tree of children chapters for each sdschap. 
 * 4- get empty sdschapters for them.
 *     4.1- leaf chapters that neither connected with phractxt nor phraftxt (selected as child = 0).
 * 	   4.2- parent chapteres that have selected as child + selected all tree = 0
 * 5- exclude empty chapters that related to minimalist or to (newly or template) sdsdoc.  
 * */

/* LiveDB: */

CREATE TEMPORARY TABLE IF NOT EXISTS useless_sdschap AS TABLE sdschap WITH NO DATA;

TRUNCATE TABLE useless_sdschap;

WITH  frozen_and_validated AS (
 SELECT sdsdoc_id, tp
	FROM sdsdoc
	WHERE tpsfrom < '1-1-2020' 
	AND sdsdoc_id > 1E8 AND tp <> 'TEMPLATE'
	
 ), 
/* get chapters of all frozen docs. */
chapters_for_frozen_documents AS (
		select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
		from fventr_sdsdoc_sdschap_subsdschap((SELECT array_agg(sdsdoc_id) FROM frozen_and_validated))						
		inner join sdschap on sdschap_id = ventr_dst_id						
		UNION ALL						
		select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
		from entr_sdsdoc_sdschap						
		inner join sdschap on sdschap_id = entr_dst_id						
		where entr_src_id  in (SELECT sdsdoc_id FROM frozen_and_validated)			
), 
/* get the tree of children chapters for each chapter.*/
chapters_tree AS (
		select ventr_src_id as grandparentsdschap_id, ventr_dst_id AS sdschap_id, ventr_tp as linktype, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
		from fventr_sdschap_sdschap_subsdschap_1((SELECT array_agg(sdschap_id) FROM chapters_for_frozen_documents))						
		inner join sdschap on sdschap_id = ventr_dst_id						
		UNION ALL						
		select entr_src_id as grandparentsdschap_id, entr_dst_id AS sdschap_id, entr_tp as linktype, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
		from entr_sdschap_sdschap						
		inner join sdschap on sdschap_id = entr_dst_id						
		where entr_src_id  in (SELECT sdschap_id FROM chapters_for_frozen_documents)	 
),
/* count the relations for each sdschap and, sum all links for each parent.*/
get_sdschap_relations_count AS (
    SELECT  nbPhactxt+nbPhaftxt AS selected_as_child, sum(nbPhactxt+nbPhaftxt) OVER (PARTITION BY grandparentsdschap_id) AS selected_all_tree, ct.*
    FROM chapters_tree ct
    INNER JOIN (SELECT sdschap_id, count(entr_id) AS nbPhactxt FROM sdschap LEFT JOIN entr_sdschap_phractxtke ON sdschap_id = entr_src_id AND entr_tp = 'SELECTED' GROUP BY sdschap_id) selected1 ON ct.sdschap_id = selected1.sdschap_id
    INNER JOIN (SELECT sdschap_id, count(entr_id) AS nbPhaftxt FROM sdschap LEFT JOIN entr_sdschap_phraftxt ON sdschap_id = entr_src_id AND entr_tp = 'SELECTED' GROUP BY sdschap_id) selected2 ON ct.sdschap_id = selected2.sdschap_id
    ORDER BY nbPhactxt+nbPhaftxt
),
get_all_empty_sdschapters AS (
	( 
	/* get all leafs that haven't relations to phractxt/phraftxt, selectd = 0 for them. */
	 SELECT sdschap_id 
	 FROM get_sdschap_relations_count WHERE selected_as_child = 0
	 EXCEPT 
 	 SELECT parentsdschap_id AS sdschap_id  FROM get_sdschap_relations_count  
	 )
 UNION 
	(
	/* get all parents that have: selected = 0 for them + selected = 0 for all their children(tree). */
	SELECT t1.grandparentsdschap_id AS sdschap_id 
	FROM get_sdschap_relations_count t1
	INNER JOIN get_sdschap_relations_count t2 ON t1.grandparentsdschap_id = t2.sdschap_id
	WHERE (t1.selected_all_tree + t2.selected_as_child) = 0 
	)
), 
/*get minimalist template. */
minimalist AS (						
	select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
	from fventr_sdsdoc_sdschap_subsdschap(ARRAY[2])						
	inner join sdschap on sdschap_id = ventr_dst_id						
	UNION ALL						
	select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
	from entr_sdsdoc_sdschap						
	inner join sdschap on sdschap_id = entr_dst_id						
	where entr_src_id  in (2)						
)
/* get all empty chapters (parents and leafs) that not in minimalist and not related to newly or template sdsdoc. */
INSERT INTO useless_sdschap SELECT s.*
	FROM get_all_empty_sdschapters empty_chapters
	INNER JOIN sdschap s ON s.sdschap_id = empty_chapters.sdschap_id 
	LEFT JOIN minimalist ON empty_chapters.sdschap_id = minimalist.sdschap_id
	LEFT JOIN entr_sdsdoc_sdschap  ess ON entr_dst_id = empty_chapters.sdschap_id AND (ess.tpsfrom > '1-1-2020' OR   ess.entr_src_id < 1E8) 
WHERE minimalist.sdschap_id IS NULL AND ess.entr_id  IS NULL ; 


/* extract useless sdschap. */
SELECT * FROM useless_sdschap;

/* extract useless-sdschap relations: */
-- der_sdschap:
SELECT der_sdschap.* FROM useless_sdschap INNER JOIN der_sdschap ON der_src_id = sdschap_id;

-- entr_sdsdoc_sdschap:
SELECT entr_sdsdoc_sdschap.* FROM useless_sdschap INNER JOIN entr_sdsdoc_sdschap ON entr_dst_id = sdschap_id;

-- entr_sdschap_sdselt:
SELECT entr_sdschap_sdselt.* FROM useless_sdschap INNER JOIN entr_sdschap_sdselt ON entr_src_id = sdschap_id;

-- entr_sdschap_sdschap
SELECT entr_sdschap_sdschap.* FROM useless_sdschap INNER JOIN entr_sdschap_sdschap ON entr_src_id = sdschap_id OR entr_dst_id = sdschap_id;


-- entr_sdschap_phractxtke:
/* here we maight get relations because we assume that empty chapters are chapters have no relations with phractxtke where tp is 'selected' */
SELECT entr_sdschap_phractxtke.* FROM useless_sdschap INNER JOIN entr_sdschap_phractxtke ON entr_src_id = sdschap_id;

-- entr_sdschap_phraftxt:
SELECT entr_sdschap_phraftxt.* FROM useless_sdschap INNER JOIN entr_sdschap_phraftxt ON entr_src_id = sdschap_id;


/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$

CREATE TEMPORARY TABLE IF NOT EXISTS useless_sdschap AS TABLE sdschap WITH NO DATA;

TRUNCATE TABLE useless_sdschap;

/* DBTps: get frozen and deleted sdsdoc before 9999-12-29 */
WITH frozen_and_validated AS (
	select sdsdoc_id, max(tpstill) as deletion_dt
	from sdsdoc
	group by sdsdoc_id
	having max(tpstill) < '9999-12-29'
	AND ((sdsdoc_id > 1E8 AND  tp <> 'TEMPLATE'))
),
/* get chapters of all frozen docs. */
chapters_for_frozen_documents AS (
		select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
		from fventr_sdsdoc_sdschap_subsdschap((SELECT array_agg(sdsdoc_id) FROM frozen_and_validated))						
		inner join sdschap on sdschap_id = ventr_dst_id						
		UNION ALL						
		select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
		from entr_sdsdoc_sdschap						
		inner join sdschap on sdschap_id = entr_dst_id						
		where entr_src_id  in (SELECT sdsdoc_id FROM frozen_and_validated)			
), 
/* get the tree of children chapters for each chapter.*/
chapters_tree AS (
		select ventr_src_id as grandparentsdschap_id, ventr_dst_id AS sdschap_id, ventr_tp as linktype, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
		from fventr_sdschap_sdschap_subsdschap_1((SELECT array_agg(sdschap_id) FROM chapters_for_frozen_documents))						
		inner join sdschap on sdschap_id = ventr_dst_id						
		UNION ALL						
		select entr_src_id as grandparentsdschap_id, entr_dst_id AS sdschap_id, entr_tp as linktype, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
		from entr_sdschap_sdschap						
		inner join sdschap on sdschap_id = entr_dst_id						
		where entr_src_id  in (SELECT sdschap_id FROM chapters_for_frozen_documents)	 
),
/* count the relations for each sdschap and, sum all links for each parent.*/
get_sdschap_relations_count AS (
    SELECT  nbPhactxt+nbPhaftxt AS selected_as_child, sum(nbPhactxt+nbPhaftxt) OVER (PARTITION BY grandparentsdschap_id) AS selected_all_tree, ct.*
    FROM chapters_tree ct
    INNER JOIN (SELECT sdschap_id, count(entr_id) AS nbPhactxt FROM sdschap LEFT JOIN entr_sdschap_phractxtke ON sdschap_id = entr_src_id AND entr_tp = 'SELECTED' GROUP BY sdschap_id) selected1 ON ct.sdschap_id = selected1.sdschap_id
    INNER JOIN (SELECT sdschap_id, count(entr_id) AS nbPhaftxt FROM sdschap LEFT JOIN entr_sdschap_phraftxt ON sdschap_id = entr_src_id AND entr_tp = 'SELECTED' GROUP BY sdschap_id) selected2 ON ct.sdschap_id = selected2.sdschap_id
    ORDER BY nbPhactxt+nbPhaftxt
),
get_all_empty_sdschapters AS (
	( 
	/* get all leafs that haven't relations to phractxt/phraftxt, selectd = 0 for them. */
	 SELECT sdschap_id 
	 FROM get_sdschap_relations_count WHERE selected_as_child = 0
	 EXCEPT 
 	 SELECT parentsdschap_id AS sdschap_id  FROM get_sdschap_relations_count  
	 )
 UNION 
	(
	/* get all parents that have: selected = 0 for them + selected = 0 for all their children(tree). */
	SELECT t1.grandparentsdschap_id AS sdschap_id 
	FROM get_sdschap_relations_count t1
	INNER JOIN get_sdschap_relations_count t2 ON t1.grandparentsdschap_id = t2.sdschap_id
	WHERE (t1.selected_all_tree + t2.selected_as_child) = 0 
	)
), 
/*get minimalist template. */
minimalist AS (						
	select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
	from fventr_sdsdoc_sdschap_subsdschap(ARRAY[2])						
	inner join sdschap on sdschap_id = ventr_dst_id						
	UNION ALL						
	select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
	from entr_sdsdoc_sdschap						
	inner join sdschap on sdschap_id = entr_dst_id						
	where entr_src_id  in (2)						
)
/* get all empty chapters (parents and leafs) that not in minimalist and not related to newly or template sdsdoc. */
INSERT INTO useless_sdschap SELECT s.*
	FROM get_all_empty_sdschapters empty_chapters
	INNER JOIN sdschap s ON s.sdschap_id = empty_chapters.sdschap_id 
	LEFT JOIN minimalist ON empty_chapters.sdschap_id = minimalist.sdschap_id
	LEFT JOIN entr_sdsdoc_sdschap  ess ON entr_dst_id = empty_chapters.sdschap_id AND (ess.tpsfrom > '1-1-2020' OR   ess.entr_src_id < 1E8) 
WHERE minimalist.sdschap_id IS NULL AND ess.entr_id  IS NULL ; 


/* extract useless sdschap. */
SELECT * FROM useless_sdschap;

/* extract useless-sdschap relations: */
-- der_sdschap:
SELECT der_sdschap.* FROM useless_sdschap INNER JOIN der_sdschap ON der_src_id = sdschap_id;

-- entr_sdsdoc_sdschap:
SELECT entr_sdsdoc_sdschap.* FROM useless_sdschap INNER JOIN entr_sdsdoc_sdschap ON entr_dst_id = sdschap_id;

-- entr_sdschap_sdselt:
SELECT entr_sdschap_sdselt.* FROM useless_sdschap INNER JOIN entr_sdschap_sdselt ON entr_src_id = sdschap_id;

-- entr_sdschap_sdschap
SELECT entr_sdschap_sdschap.* FROM useless_sdschap INNER JOIN entr_sdschap_sdschap ON entr_src_id = sdschap_id OR entr_dst_id = sdschap_id;


-- entr_sdschap_phractxtke:
/* here we maight get relations because we assume that empty chapters are chapters have no relations with phractxtke where tp is 'selected' */
SELECT entr_sdschap_phractxtke.* FROM useless_sdschap INNER JOIN entr_sdschap_phractxtke ON entr_src_id = sdschap_id;

-- entr_sdschap_phraftxt:
SELECT entr_sdschap_phraftxt.* FROM useless_sdschap INNER JOIN entr_sdschap_phraftxt ON entr_src_id = sdschap_id;

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');




