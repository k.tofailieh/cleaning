/*
 1-check for the tables that have ns column without index on it.
 2-create index for them.
 3-the same for DPTps.
*/

/* get the tables that have ns column. */
WITH get_tables_have_ns_column AS
         (SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name
          FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
                   INNER JOIN INFORMATION_SCHEMA.TABLES
                              ON (TABLES.table_schema, TABLES.table_name) =
                                 (columns.table_schema, columns.table_name)
                                  AND TABLES.table_type = 'BASE TABLE'
                                  AND COLUMNS.column_name = 'ns'),

    /* get the tables that have index on ns column.*/
     get_tables_have_no_index_ns AS
         (SELECT schemaname, tablename, indexname, indexdef
          FROM pg_indexes
          WHERE schemaname IN ('public', 'anx')
            AND NOT indexdef LIKE ('%UNIQUE%')
            AND SPLIT_PART(indexdef, '(', 2) LIKE ANY (VALUES ('%ns,%'), ('%,ns%'), ('%ns)%')))
/* exclude tables that have not index. */
SELECT table_schema, table_name
FROM get_tables_have_ns_column
         LEFT JOIN get_tables_have_no_index_ns ON schemaname = table_schema AND tablename = table_name
WHERE tablename IS NULL;

/* create index.  */
CREATE INDEX endpt_ns_idx ON public.endpt USING btree (ns);
CREATE INDEX reg_ns_idx ON public.reg USING btree (ns);
CREATE INDEX asset_ns_idx ON public.asset USING btree (ns);
CREATE INDEX customerorder_ns_idx ON public.customerorder USING btree (ns);
CREATE INDEX doc_ns_idx ON public.doc USING btree (ns);
CREATE INDEX profile_ns_idx ON public.profile USING btree (ns);
CREATE INDEX study_ns_idx ON public.study USING btree (ns);
CREATE INDEX feedback_ns_idx ON public.feedback USING btree (ns);
CREATE INDEX orderitem_ns_idx ON public.orderitem USING btree (ns);
CREATE INDEX plcsys_ns_idx ON public.plcsys USING btree (ns);
CREATE INDEX project_ns_idx ON public.project USING btree (ns);
CREATE INDEX society_ns_idx ON public.society USING btree (ns);
CREATE INDEX subscription_ns_idx ON public.subscription USING btree (ns);
CREATE INDEX phrac_ns_idx ON public.phrac USING btree (ns);
CREATE INDEX payment_ns_idx ON public.payment USING btree (ns);
CREATE INDEX ns_ns_idx ON public.ns USING btree (ns);
CREATE INDEX wfprocess_ns_idx ON public.wfprocess USING btree (ns);
CREATE INDEX phraftxt_ns_idx ON public.phraftxt USING btree (ns);
CREATE INDEX txt_ns_idx ON public.txt USING btree (ns);
CREATE INDEX txtke_ns_idx ON public.txtke USING btree (ns);
CREATE INDEX wftask_ns_idx ON public.wftask USING btree (ns);
CREATE INDEX searchdesc_ns_idx ON public.searchdesc USING btree (ns);
CREATE INDEX tm_ns_idx ON public.tm USING btree (ns);

------------------------------------------------------------------------------------------------------------------------

/*
######  ######  #######
#     # #     #    #    #####   ####
#     # #     #    #    #    # #
######  #     #    #    #    #  ####
#     # #     #    #    #####       #
#     # #     #    #    #      #    #
######  ######     #    #       ####
*/

SELECT dblink_connect('xmat-tps',
                      'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
/* get the tables that have ns column. */
WITH get_tables_have_ns_column AS
        (SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name
    FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
        INNER JOIN INFORMATION_SCHEMA.TABLES
        ON (TABLES.table_schema, TABLES.table_name) =
            (columns.table_schema, columns.table_name)
            AND TABLES.table_type = 'BASE TABLE'
            AND COLUMNS.column_name = 'ns'),

    /* get the tables that have index on ns column.*/
    get_tables_have_no_index_ns AS
            (SELECT schemaname, tablename, indexname, indexdef
        FROM pg_indexes
        WHERE schemaname IN ('public', 'anx')
            AND NOT indexdef LIKE ('%UNIQUE%')
            AND SPLIT_PART(indexdef, '(', 2) LIKE ANY (VALUES ('%ns,%'), ('%,ns%'), ('%ns)%')))
    /* exclude tables that have not index. */
SELECT table_schema, table_name FROM get_tables_have_ns_column LEFT JOIN get_tables_have_no_index_ns ON schemaname = table_schema AND tablename = table_name
WHERE tablename IS NULL;

/* create index.  */
CREATE INDEX endpt_ns_idx ON public.endpt USING btree (ns);
CREATE INDEX reg_ns_idx ON public.reg USING btree (ns);
CREATE INDEX asset_ns_idx ON public.asset USING btree (ns);
CREATE INDEX customerorder_ns_idx ON public.customerorder USING btree (ns);
CREATE INDEX doc_ns_idx ON public.doc USING btree (ns);
CREATE INDEX profile_ns_idx ON public.profile USING btree (ns);
CREATE INDEX study_ns_idx ON public.study USING btree (ns);
CREATE INDEX feedback_ns_idx ON public.feedback USING btree (ns);
CREATE INDEX orderitem_ns_idx ON public.orderitem USING btree (ns);
CREATE INDEX plcsys_ns_idx ON public.plcsys USING btree (ns);
CREATE INDEX project_ns_idx ON public.project USING btree (ns);
CREATE INDEX society_ns_idx ON public.society USING btree (ns);
CREATE INDEX subscription_ns_idx ON public.subscription USING btree (ns);
CREATE INDEX phrac_ns_idx ON public.phrac USING btree (ns);
CREATE INDEX payment_ns_idx ON public.payment USING btree (ns);
CREATE INDEX ns_ns_idx ON public.ns USING btree (ns);
CREATE INDEX wfprocess_ns_idx ON public.wfprocess USING btree (ns);
CREATE INDEX phraftxt_ns_idx ON public.phraftxt USING btree (ns);
CREATE INDEX txt_ns_idx ON public.txt USING btree (ns);
CREATE INDEX txtke_ns_idx ON public.txtke USING btree (ns);
CREATE INDEX wftask_ns_idx ON public.wftask USING btree (ns);
CREATE INDEX searchdesc_ns_idx ON public.searchdesc USING btree (ns);
CREATE INDEX tm_ns_idx ON public.tm USING btree (ns);

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');



