
WITH get_tables_have_ns_column AS
         (SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name
          FROM information_schema.columns
                   INNER JOIN INFORMATION_SCHEMA.TABLES
                              ON (TABLES.table_schema, TABLES.table_name) =
                                 (columns.table_schema, columns.table_name)
                                  AND TABLES.table_type = 'BASE TABLE'
                                  AND COLUMNS.column_name = 'ns')
select * from get_tables_have_ns_column;

/*
alter tables that contains ns column.
update ns:
    1- set as format(1)(@org).
    2- force '*' if ns values either '' or null.
*/

/* Live DB. */

update endpt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update fiche set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update nomclit set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update plc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update reg set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update app set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update asset set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update customerorder set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update doc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update profile set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update study set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update feedback set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update orderitem set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update part set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update plcsys set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update project set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update sdsdoc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update society set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update subscription set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update phrac set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update payment set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update ns set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update lov set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update ext set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update extfld set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update wfprocess set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update phraftxt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update txt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update txtke set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update wftask set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update hrframework set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update searchdesc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update tm set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);




------------------------------------------------------------------------------------------------------------------------

/*
######  ######  #######
#     # #     #    #    #####   ####
#     # #     #    #    #    # #
######  #     #    #    #    #  ####
#     # #     #    #    #####       #
#     # #     #    #    #      #    #
######  ######     #    #       ####
*/

SELECT dblink_connect('xmat-tps',
                      'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$

update endpt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update fiche set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update nomclit set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update plc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update reg set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update app set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update asset set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update customerorder set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update doc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update profile set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update study set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update feedback set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update orderitem set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update part set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update plcsys set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update project set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update sdsdoc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update society set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update subscription set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update phrac set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update payment set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update ns set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update lov set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update ext set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update extfld set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update wfprocess set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update phraftxt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update txt set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update txtke set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update wftask set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update hrframework set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update searchdesc set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);
update tm set ns = split_part(coalesce(nullif(ns,''),'*'), '/', 1);

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');


select * from  anx.inci where inciname like 'kh%'

insert into entr_inci_incitranslation

select * from anx.entr_inci_incitranslation where entr_src_id = 73635

select * from anx.incitranslation where tuv_seg like 'kh%'