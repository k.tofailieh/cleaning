/* Get nomclit in both ways to compare. */
with formula_process_nomclit as -- fiche[FORMULA|BATCH] ----> process ----> process[PHASE] ----> nomclit
         (select m_id, epn.entr_dst_id as nomclit_id
          from fiche
               inner join entr_fiche_process efp
                              on fiche.m_id = efp.entr_src_id and efp.entr_tp = 'COSMETIC_MANUFACTURING_PROCESS'
               inner join entr_process_process epp on efp.entr_dst_id = epp.entr_src_id and epp.entr_tp = 'USE'
               inner join entr_process_nomclit epn on epp.entr_dst_id = epn.entr_src_id and epn.entr_tp = 'USE'
          where upper(tp) in
                ('PERFUME_CONCENTRATE_BATCH', 'COSMETIC_FORMULA', 'COSMETIC_FORMULA_TEST', 'COSMETIC_PILOT_BATCH')),

     formula_nomclit as -- fiche[FORMULA|BATCH] ----> nomclit
         (select m_id, entr_tp, entr_dst_id as nomclit_id
          from fiche
               left join entr_fiche_nomclit efn
                             on fiche.m_id = efn.entr_src_id and entr_tp in ('DIRECT_COMPO_FICHE_RM', 'DIRECT_COMPO')
          where upper(tp) in
                ('PERFUME_CONCENTRATE_BATCH', 'COSMETIC_FORMULA', 'COSMETIC_FORMULA_TEST', 'COSMETIC_PILOT_BATCH'))
insert
into import.import_line(imp_ref, value_id1, value_id2, value1)
select 'fix-formula-nomclit', formula_process_nomclit.m_id, formula_process_nomclit.nomclit_id,
       'DIRECT_COMPO_FICHE_RM' as entr_tp
from formula_process_nomclit
     left join formula_nomclit using (m_id, nomclit_id)
where formula_nomclit.nomclit_id is null -- missed entr_fiche_nomclit
;

/* Insert missed relation's to database. */
insert into entr_fiche_nomclit(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('seq_entr_fiche_nomclit_id'), value_id1, value_id2, value1, 'inserted to keep data consistent.'
from import.import_line
where imp_ref = 'fix-formula-nomclit'
  and value_id1 is not null
  and value_id2 is not null
group by value_id1, value_id2, value1
on conflict (entr_src_id, entr_tp, entr_dst_id) do nothing;

------------------------------------------------------------------------------------------------------------------------
begin;
/* Get nomclit for both ways to compare. */
with formula_process_nomclit as -- fiche[FORMULA|BATCH] ----> process ----> process[PHASE] ----> nomclit
         (select m_id, epn.entr_dst_id as nomclit_id, efp.entr_dst_id as process_id
          from fiche
               left join entr_fiche_process efp
                             on fiche.m_id = efp.entr_src_id and efp.entr_tp = 'COSMETIC_MANUFACTURING_PROCESS'
               left join entr_process_process epp on efp.entr_dst_id = epp.entr_src_id and epp.entr_tp = 'USE'
               left join entr_process_nomclit epn on epp.entr_dst_id = epn.entr_src_id and epn.entr_tp = 'USE'
          where upper(tp) in
                ('PERFUME_CONCENTRATE_BATCH', 'COSMETIC_FORMULA', 'COSMETIC_FORMULA_TEST', 'COSMETIC_PILOT_BATCH')),
     formula_nomclit as -- fiche[FORMULA|BATCH] ----> nomclit
         (select fiche.m_id, efn.entr_tp, efn.entr_dst_id as nomclit_id, fiche.tp
          from fiche
               inner join entr_fiche_nomclit efn
                              on fiche.m_id = efn.entr_src_id and entr_tp in ('DIRECT_COMPO_FICHE_RM', 'DIRECT_COMPO')
               left join  entr_nomclit_fiche enf on enf.entr_src_id = efn.entr_dst_id and
                                                    enf.entr_tp = 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA'
               left join  fiche rm on enf.entr_dst_id = rm.m_id
          where upper(fiche.tp) in
                ('PERFUME_CONCENTRATE_BATCH', 'COSMETIC_FORMULA', 'COSMETIC_FORMULA_TEST', 'COSMETIC_PILOT_BATCH'))
insert
into import.import_line (imp_ref, value_id1, value_id2, value_id3)

-- get formula and nomclit Id's for that have missed porcess.
select 'fix-formula-process-nomclit',
       formula_nomclit.m_id as value_id1,
       formula_nomclit.nomclit_id as value_id2,
       process_id as value_id3
from formula_nomclit
     left join formula_process_nomclit using (m_id, nomclit_id)
where process_id is null
;


/* create new process_id for formulas that don`t have process. */
with distinct_process as (select value_id1 as formula_id, -nextval('seq_process_id') process_id
                          from import.import_line
                          where imp_ref = 'fix-formula-process-nomclit'
                            and value_id3 is null
                          group by value_id1)
update import.import_line
set value_id3 = distinct_process.process_id
from distinct_process
where imp_ref = 'fix-formula-process-nomclit'
  and value_id1 = formula_id
  and value_id3 is null
;

/* create new process: default process. */
insert into process(process_id, process_tp, process_cmt, process_co)
select abs(value_id3), 'COSMETIC_MANUFACTURING_PROCESS', 'Default Phase',
       'Inserted as default process to keep data consistent.'
from import.import_line
where imp_ref = 'fix-formula-process-nomclit'
  and coalesce(value_id3, 0) < 0
group by value_id3
;

/* create new relation for: formula -> process */
insert into entr_fiche_process (entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('seq_entr_fiche_process_id') as entr_id,
       abs(value_id1) as entr_src_id,
       abs(value_id3) as entr_dst_id,
       'COSMETIC_MANUFACTURING_PROCESS' as entr_tp,
       'Default Phase.' as entr_co
from import.import_line
where imp_ref = 'fix-formula-process-nomclit'
  and coalesce(value_id1, 0) > 0 -- current formula.
  and coalesce(value_id3, 0) < 0 -- new phase.
group by value_id1, value_id3
;


/* create new phase_id for: formula-processes. */
with distinct_process_phases as (select value_id1 as formula_id,
                                        value_id3 as process_id,
                                        -nextval('seq_process_id') phase_id
                                 from import.import_line
                                 where imp_ref = 'fix-formula-process-nomclit'
                                   and value_id4 is null
                                 group by value_id1, value_id3)
update import.import_line
set value_id4 = distinct_process_phases.phase_id
from distinct_process_phases
where imp_ref = 'fix-formula-process-nomclit'
  and value_id1 = formula_id
  and value_id3 = process_id
;

/* create new phase: default phase. */
insert into process (process_id, process_li, process_tp, process_co)
select abs(value_id4), 'Phase by default', 'COSMETIC_PHASE',
       'This phase has been created by default. It contains the existing composition'
from import.import_line
where imp_ref = 'fix-formula-process-nomclit'
  and coalesce(value_id4, 0) < 0
group by value_id4
on conflict do nothing
;

/* create new relation for: process -> phase */
insert into entr_process_process (entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('seq_entr_process_process_id') as entr_id,
       abs(value_id3) as entr_src_id,
       abs(value_id4) as entr_dst_id,
       'USE' as entr_tp,
       'Default phase.' as entr_co
from import.import_line
where imp_ref = 'fix-formula-process-nomclit'
  and coalesce(value_id3, 0) < 0 -- new process
  and coalesce(value_id4, 0) < 0 -- new phase
group by value_id3, value_id4
on conflict (entr_src_id, entr_tp, entr_dst_id) do nothing
;

/* create new relation for: phase -> nomclit. */
insert into entr_process_nomclit(
    entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('seq_entr_process_nomclit_id'),
       abs(value_id4),
       value_id2,
       'USE',
       'Default phase.'
from import.import_line
where imp_ref = 'fix-formula-process-nomclit'
  and coalesce(value_id4, 0) < 0
  and coalesce(value_id2, 0) > 0
group by value_id4, value_id2
;


