-- get relations
SELECT epp.*
from entr_phractxtke_phractxt epp
         inner join phractxtke on phractxtke_id = epp.entr_src_id
         inner join phractxt on phractxt_id = epp.entr_dst_id
WHERE entr_tp isnull;

-- get duplcated relations and delete them.
with get_duplicated_relations as (
    SELECT 
           entr_id,                                                      
           entr_tp,
           entr_src_id,
           entr_dst_id,
           row_number()
           over (partition by entr_src_id, entr_dst_id) as row_num
           FROM entr_phractxtke_phractxt
    where entr_tp isnull
)
-- if you want to see the result just uncomment the following line and comment the delete statement:
--select * from get_duplicated_relations;
delete
from entr_phractxtke_phractxt
where entr_id in (select dr.entr_id from get_duplicated_relations dr where dr.row_num > 1);

-- update the life database:
update entr_phractxtke_phractxt set entr_tp = 'TRANSLATION' where entr_tp isnull;

-- add not-null constraint on tables:
alter table entr_phractxtke_phractxt alter column entr_tp set not null;

-- test the constraint:
insert into entr_phractxtke_phractxt(entr_dst_id, entr_src_id) values (6114, 281);

select * from entr_phractxtke_phractxt where entr_src_id = 6114 and entr_dst_id = 281;
