
update import.import_line set internal_id = null
where imp_id in (select imp_id );

select nextval('seq_fiche_id');
select internal_id,value1,* from import.import_line where imp_ref=:imp_ref;

update import.import_line
set internal_id=515454
where imp_ref=:imp_ref;

with duplicated as (
    select imp_id,internal_id, row_number() over (PARTITION BY internal_id) rn  from import.import_line
    where imp_ref = :imp_ref
)
update import.import_line
set internal_id = null
from duplicated
where duplicated.imp_id = import_line.imp_id and duplicated.rn > 1


where imp_id in (select imp_id from duplicated where rn > 1)


---------------------------------------------------------


/*
 * 1- get duplicated substances
 * 2- original substance are the most informative ones.
 * 3- keep the original in the beginning of each partition.
 * 4- align each original substance with all it's duplications(side by side).
 * 5- call reindex-delete function to reindex all of them and then delete them.
 * */


/* duplicated substances is temporal table contains the original substance aligned with it's duplications side by side. */
drop table if exists duplicated_substances;
create temporary table duplicated_substances
(
    original_id   bigint,
    original_naml varchar,
    original_cas  varchar,
    original_ec   varchar,
    copy_id       bigint,
    copy_naml     varchar,
    copy_cas      varchar,
    copy_ec       varchar
);


with substance_duplicates as
         (
/* get the duplicated substances with prtitioning using (naml,cas,ec) to manipulate all situations of duplication.*/
             select sub_id,
                    naml,
                    cas,
                    ec,
                    mf,
                    count(sub_id) over (partition by naml)                                                                as name_cnt,
                    row_number()
                    over (partition by naml order by nullif(cas, ''), nullif(ec, '') nulls last)                          as name_nb,
                    count(sub_id) over (partition by naml,cas)                                                            as cas_cnt,
                    row_number()
                    over (partition by naml,cas order by nullif(ec, '') nulls last)                                       as cas_nb,
                    count(sub_id) over (partition by naml,ec)                                                             as ec_cnt,
                    row_number()
                    over (partition by naml,ec order by nullif(cas, '') nulls last)                                       as ec_nb,
                    count(sub_id) over (partition by naml,cas,ec)                                                         as total_cnt,
                    row_number()
                    over (partition by naml,cas,ec order by nullif(naml, ''), nullif(cas, ''), nullif(ec, '') nulls last) as total_nb
             from substances
             order by naml),
     get_original_with_copies as (
/* this subquery is to align originals with copies (original substance is the most informative one)*/
         select original.sub_id as o_id,
                original.naml   as o_naml,
                original.cas    as o_cas,
                original.ec     as o_ec,
                copies.sub_id   as c_id,
                copies.naml     as c_naml,
                copies.cas      as c_cas,
                copies.ec       as c_ec
         from (select *
               from substance_duplicates
               where name_cnt > 1 and (name_nb = 1 and cas_nb = 1 and ec_nb = 1 and total_nb = 1)) original
              inner join (select *
                          from substance_duplicates
                          where name_cnt > 1 and (total_nb > 1 or cas_nb > 1 or ec_nb > 1)) copies
                             on original.naml = copies.naml and original.sub_id <> copies.sub_id and
                                ((original.cas = copies.cas or original.ec = copies.ec) or
                                 (coalesce(copies.cas, '') = '' and coalesce(copies.ec, '') = '')))

insert
into duplicated_substances
select o_id, o_naml, o_cas, o_ec, c_id, c_naml, c_cas, c_ec
from get_original_with_copies

/* extract result: */
select *
from duplicated_substances;

/*call reindex-delete function.*/
select reindex_delete_entity(
               'substances', /*old_ids are the copies*/(select array_agg(copy_id) from duplicated_substances), /* new_ids are the originals.*/
               (select array_agg(original_id) from duplicated_substances))

/* Drop temporal table.*/
           drop table if exists duplicated_substances;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* inci duplications */

drop table if exists duplicated_incis;
create temporary table duplicated_incis
(
    original_id   bigint,
    original_naml varchar,
    original_cas  varchar,
    copy_id       bigint,
    copy_naml     varchar,
    copy_cas      varchar
);


with inci_duplicates as (select inci_id,
                                inciname,
                                cas,
                                anx_st,
                                count(*) over (partition by inciname)                                         as cnt,
                                row_number() over (partition by inciname order by nullif(cas, '') nulls last) as rownum
                         from anx.inci
                         where inciname is not null),
     get_original_with_copies as (select original.inci_id  as o_inci_id,
                                         original.inciname as o_inciname,
                                         original.cas      as o_cas,
                                         copies.inci_id    as c_inci_id,
                                         copies.inciname   as c_incianme,
                                         copies.cas        as c_cas
                                  from (select * from inci_duplicates where rownum = 1) original
                                       inner join (select * from inci_duplicates where rownum > 1) copies using (inciname))
insert
into duplicated_incis
select *
from get_original_with_copies;

/* extract result: */
select *
from duplicated_incis;

/* call reindex-delete function.*/
select reindex_delete_entity(
               'inci', /*old_ids are the copies*/(select array_agg(copy_id) from duplicated_incis), /* new_ids are the originals.*/
               (select array_agg(original_id) from duplicated_incis))

/* drop temporal table */
           drop table if exists duplicated_incis;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
