
/* get ext_id for forms: */
select *
from ext
where ke = 'COSMETOVIGILANCE_FEEDBACK_FORM' and tp = 'TEMPLATE' and st = 'VALID' and tptp = 'Feedback' and lg = 'en';

/* merge all duplications in one record (we kept the newest one)*/
select reindex_delete_entity('ext', 48580,  59601);
select reindex_delete_entity('ext', 48257,  59601);
