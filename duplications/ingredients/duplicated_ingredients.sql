-- temporary table to save the Id's of the duplications with the base_id for each duplicated record.
create temporary table ingredients_duplications
(
    m_id      bigint,
    base_m_id bigint
);

-- get duplications ingredient duplications:
with duplicated_ingredients as
         (select fiche.m_id,
                 row_number()
                 over (partition by inci_id, fiche.tp, split_part(ns, '/', 1), entr_tp order by fiche.tpsfrom) as rn,
                 first_value(m_id) over (partition by inci_id, fiche.tp, split_part(ns, '/', 1), entr_tp order by fiche.tpsfrom) as base_m_id,
                 inciname, inci.cas, fiche.tpsfrom as ing_cr_dt, entr_fiche_inci.tpsfrom as entr_fiche_inci_cr_dt, ns,
                 count(*) over (partition by inci_id, fiche.tp, split_part(ns, '/', 1), entr_tp) as cnt
          from fiche
               inner join anx.entr_fiche_inci on entr_src_id = m_id
               inner join anx.inci on inci_id = entr_dst_id
          where fiche.tp in ('INGREDIENT', 'ALLERGEN')
            and anx_st = 'VALID'
            and fiche.active in (1, 2)
            and upper(trim(split_part(ns, '/', 1))) = upper(trim(:ns)) -- you can comment this part if you want run it for all ns
          order by ing_cr_dt desc, inciname desc, rn)
-- insert the Ids of duplications to a temporary table to reindex them.
-- to check the result of this query just comment the "insert" line and put "*" in "select" line.
insert
    into ingredients_duplications(m_id, base_m_id)
select m_id, base_m_id
from duplicated_ingredients
where cnt > 1 and m_id != base_m_id -- exclude the first(base) record from result set.
;

-- need confirmation to run this function:
select reindex_delete_entity('fiche', m_id, base_m_id)
from ingredients_duplications
;

delete
from ingredients_duplications;