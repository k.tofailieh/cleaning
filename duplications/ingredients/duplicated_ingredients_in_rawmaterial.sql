select rm.m_id, rm.m_ref, nomclit_id, nomclit.conc_direct_lbound, nomclit.conc_direct_ubound, ing.m_id, ing.m_ref,ing.tp,
       entr_fiche_inci.entr_tp,inci_id, inciname
from fiche ing
     inner join anx.entr_fiche_inci on entr_fiche_inci.entr_src_id = ing.m_id
     inner join anx.inci on inci_id = entr_fiche_inci.entr_dst_id
where ing.tp in( 'RAW_MATERIAL',