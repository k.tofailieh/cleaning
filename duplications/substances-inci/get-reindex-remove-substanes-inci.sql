
/* 
 * 1- get duplicated substances 
 * 2- original substance are the most informative ones.
 * 3- keep the original in the beginning of each partition.
 * 4- align each original substance with all it's duplications(side by side).
 * 5- call reindex-delete function to reindex all of them and then delete them.  
 * */


/* duplicated substances is temporal table contains the original substance aligned with it's duplications side by side. */
DROP TABLE IF EXISTS duplicated_substances;
CREATE TEMPORARY TABLE duplicated_substances(original_id bigint , original_naml varchar, original_cas varchar, original_ec varchar, copy_id bigint, copy_naml varchar, copy_cas varchar, copy_ec varchar);


WITH substance_duplicates AS
(
/* get the duplicated substances with prtitioning using (naml,cas,ec) to manipulate all situations of duplication.*/
	SELECT sub_id ,naml, cas, ec, mf,
	count(sub_id) OVER (PARTITION BY naml) AS name_cnt, ROW_NUMBER() OVER (PARTITION BY naml ORDER BY NULLIF(cas, ''), NULLIF(ec, '') NULLS LAST) AS name_nb,
	count(sub_id) OVER (PARTITION BY naml,cas ) AS cas_cnt, ROW_NUMBER() OVER (PARTITION BY naml,cas ORDER BY NULLIF(ec, '') NULLS LAST) AS cas_nb, 
	count(sub_id) OVER (PARTITION BY naml,ec)  AS ec_cnt, ROW_NUMBER() OVER (PARTITION BY naml,ec ORDER BY NULLIF(cas, '') NULLS LAST) AS ec_nb,
	count(sub_id) OVER (PARTITION BY naml,cas,ec ) AS total_cnt, ROW_NUMBER() OVER (PARTITION BY naml,cas,ec ORDER BY   NULLIF(naml, ''), NULLIF(cas, ''), NULLIF(ec, '') NULLS LAST) AS total_nb
FROM substances ORDER BY naml 
),
get_original_with_copies AS (
/* this subquery is to align originals with copies (original substance is the most informative one)*/
 SELECT original.sub_id AS o_id, original.naml AS o_naml, original.cas AS o_cas, original.ec AS o_ec,
 		copies.sub_id AS c_id, copies.naml AS c_naml, copies.cas AS c_cas, copies.ec AS c_ec 
 FROM (SELECT * FROM substance_duplicates WHERE name_cnt > 1 AND (name_nb = 1 AND cas_nb = 1 AND ec_nb = 1 AND total_nb = 1)) original
 INNER JOIN (SELECT * FROM substance_duplicates WHERE name_cnt > 1 AND (total_nb > 1 OR cas_nb > 1 OR ec_nb > 1)) copies
 ON original.naml = copies.naml AND original.sub_id <> copies.sub_id AND ((original.cas = copies.cas OR original.ec = copies.ec) OR (COALESCE(copies.cas, '') = '' AND COALESCE(copies.ec, '') = ''))
 )
 
INSERT INTO duplicated_substances SELECT o_id, o_naml, o_cas, o_ec, c_id, c_naml, c_cas, c_ec FROM get_original_with_copies;

/* extract result: */
SELECT * FROM duplicated_substances;

/*call reindex-delete function.*/
SELECT reindex_delete_entity('substances', /*old_ids are the copies*/(SELECT array_agg(copy_id) FROM duplicated_substances), /* new_ids are the originals.*/ (SELECT array_agg(original_id) FROM duplicated_substances) );

/* Drop temporal table.*/
DROP TABLE IF EXISTS duplicated_substances;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* inci duplications */

DROP TABLE IF EXISTS duplicated_incis;
CREATE TEMPORARY TABLE duplicated_incis(original_id bigint , original_naml varchar, original_cas varchar, copy_id bigint, copy_naml varchar, copy_cas varchar);


WITH inci_duplicates AS(
    SELECT inci_id, inciname, cas, anx_st, count(*) OVER (PARTITION BY inciname) AS cnt, ROW_NUMBER() OVER (PARTITION BY inciname ORDER BY NULLIF(cas,'') NULLS LAST) AS rowNum
FROM anx.inci
WHERE inciname IS NOT NULL
),
get_original_with_copies AS (
SELECT original.inci_id AS o_inci_id, original.inciname AS o_inciname, original.cas AS o_cas, copies.inci_id AS c_inci_id, copies.inciname AS c_incianme, copies.cas AS c_cas  
FROM (SELECT * FROM inci_duplicates WHERE rowNum = 1) original
INNER JOIN (SELECT * FROM inci_duplicates WHERE rowNum > 1) copies USING(inciname)
)
INSERT INTO duplicated_incis SELECT * FROM get_original_with_copies;

/* extract result: */
SELECT * FROM duplicated_incis;

/* call reindex-delete function.*/
SELECT reindex_delete_entity('inci', /*old_ids are the copies*/(SELECT array_agg(copy_id) FROM duplicated_incis), /* new_ids are the originals.*/ (SELECT array_agg(original_id) FROM duplicated_incis) );


/* drop temporal table */
DROP TABLE IF EXISTS duplicated_incis;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
