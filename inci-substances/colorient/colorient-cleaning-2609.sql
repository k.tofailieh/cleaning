/* -upload file in import.import_line with this specification:
    value1: CAS,
    value2: Colorant EU Name,
    value3: EU reg (colorant),
    value4: EU Reg (Hair dye),
    value5: EU Reg (UV Filter),
    value6: Colorant US Name,
    value7: Function,
    value8: US reg (colorant),
    value9: US reg (UV filter),
    value10: Comment

*/
-- imp_ref: test-colorient-cleaning

/*
Get Existing INCI's (match by provided inciname).
    -inci_id --> value_id1.
    -if it is appeared many times in the file -> update first one and create others.
*/
with matched_colorient as
         (select inci_id, imp_id, row_number() over (partition by value2) as inci_rn,
                 value2, value6, value7
          from import.import_line
               inner join anx.inci on trim(upper(inciname)) = trim(upper(value2))
          where imp_ref = :imp_ref and anx_st = 'VALID'),
     inci_ids as (select case when inci_rn = 1 then inci_id else -nextval('anx.seq_inci_id') end as inci_id,
                         imp_id, value2, value6
                  from matched_colorient)
update import.import_line
set value_id1 = inci_id
from inci_ids
where inci_ids.imp_id = import_line.imp_id
;
-- 142, 5 of them are negatives. (on saka).

/* Do Matching Using Cas Number. */
with translations as (select entr_src_id, upper(trim(tuv_seg)) as tuv_seg
                      from anx.entr_inci_incitranslation
                           left join anx.incitranslation
                                         on entr_inci_incitranslation.entr_dst_id =
                                            incitranslation.incitranslation_id and country in ('EU', 'US', 'CA')
                      group by entr_src_id, tuv_seg),
     matched_colorient as
         (select inci_id, imp_id, cas, value2, translations.tuv_seg,
                 row_number() over (partition by imp_id, value2 order by translations.tuv_seg nulls last) as inci_rn,
                 count(*) over (partition by imp_id, value2) as matches_cnt
          from import.import_line
               inner join anx.inci on trim(upper(cas)) = trim(upper(value1))
               left join  translations
                              on inci_id = translations.entr_src_id and upper(trim(tuv_seg)) = upper(trim(value2))
          where imp_ref = :imp_ref and value_id1 is null and anx_st = 'VALID')
update import.import_line
set value_id1 = inci_id
from matched_colorient
where matched_colorient.imp_id = import_line.imp_id
  and value_id1 is null
  and (matches_cnt = 1 or (tuv_seg is not null and inci_rn = 1))
;

/*
Get Existing Translations (EU, US, CA)
    -incitransaltoion(US) --> value_id2
    -incitransaltoion(EU) --> value_id3
    -incitransaltoion(CA) --> value_id4
*/
with incitranslations as (select imp_id, value_id1, value1, value2, value6, incitranslation_id, tuv_seg, country
                          from import.import_line
                               left join anx.entr_inci_incitranslation
                                             on value_id1 = entr_inci_incitranslation.entr_src_id
                                             -- and entr_tp = 'TRANSLATION'
                               left join anx.incitranslation on entr_inci_incitranslation.entr_dst_id =
                                                                incitranslation.incitranslation_id and
                                                                country in ('EU', 'US', 'CA')
                          where imp_ref = :imp_ref and value_id1 is not null),
     incitranslaton_ids as (select imp_id,
                                   value_id1,
                                   coalesce(max(case when country = 'US' then incitranslation_id else null end),
                                            -nextval('anx.seq_incitranslation_id')) as value_id2,
                                   coalesce(max(case when country = 'EU' then incitranslation_id else null end),
                                            -nextval('anx.seq_incitranslation_id')) as value_id3,
                                   coalesce(max(case when country = 'CA' then incitranslation_id else null end),
                                            -nextval('anx.seq_incitranslation_id')) as value_id4
                            from incitranslations
                            group by imp_id, value_id1)
update import.import_line
set value_id2 = incitranslaton_ids.value_id2,
    value_id3 = incitranslaton_ids.value_id3,
    value_id4 = incitranslaton_ids.value_id4
from incitranslaton_ids
where imp_ref = :imp_ref
  and import_line.imp_id = incitranslaton_ids.imp_id
  and import_line.value_id1 is not null;

------------------------------------------------------------------------------------------------------------------------

/*
Get formatted inciname like CI **** (****).
   -for example: CI 77947 (Zinc oxide)
   -when EU name is equal to US name -> proper name is EU part only.
*/
with proper_inciname as
         (select imp_id, case when trim(upper(value6)) != trim(upper(value2)) then value6 else null end as inciname
          from import.import_line
          where imp_ref = :imp_ref and value_id1 is not null)
update import.import_line
set value11 = value2 || coalesce(' (' || nullif(inciname, '') || ')', '')
from proper_inciname
where imp_ref = :imp_ref
  and import_line.imp_id = proper_inciname.imp_id
;


/*
Get the proper incitranslation fro USA.
    - if empty or null --> "⛔ Not authorized in the USA"
*/
with proper_us_incitranlation as
         (select imp_id, value_id2,
                 coalesce(nullif(trim(value6), ''), '⛔ Not authorized in the USA') as us_incitranslation
          from import.import_line
          where imp_ref = :imp_ref and value_id1 is not null)
update import.import_line
set value12 = us_incitranslation
from proper_us_incitranlation
where imp_ref = :imp_ref
  and import_line.imp_id = proper_us_incitranlation.imp_id
  and value_id1 is not null
;

/*
Get the proper incitranslation for EU.
    - if empty or null --> "⛔ Not authorized in the EU"
*/
with proper_eu_incitranlation as
         (select imp_id, value_id3, value12,
                 coalesce(nullif(trim(value2), ''), '⛔ Not authorized in the EU') as eu_incitranslation
          from import.import_line
          where imp_ref = :imp_ref and value_id1 is not null)
update import.import_line
set value13 = eu_incitranslation
from proper_eu_incitranlation
where imp_ref = :imp_ref
  and import_line.imp_id = proper_eu_incitranlation.imp_id
  and value_id1 is not null
;
-- 150

/* Get US translation as inciname for empty EU translations. */
-- value12 contains US translation.
update import.import_line
set value11 = trim(value12)
where imp_ref = :imp_ref
  and value13 = '⛔ Not authorized in the EU'
  and value_id1 is not null
  and value11 <> value12
;

------------------------------------------------------------------------------------------------------------------------

/*
Update inciname for colorient inci's.
    -No matter what is the current value.
*/
update anx.inci
set inciname = trim(value11),
    anx_co   = 'Colorient update test.'
from import.import_line
where imp_ref = :imp_ref and value_id1 = inci_id and coalesce(value11, '') != ''
;

/*
Insert new INCI's. This case is found when the same inciname has many colorient values
--> Update the first one and insert the second one.
    -example: CI 15850 (Red 6), CI 15850 (Red 7).
*/
insert into anx.inci(inci_id, inciname, cas, anx_st, tp, anx_co)
select abs(value_id1), trim(value11), trim(split_part(value1, '/', 1)), 'VALID', 'INCI', ''
from import.import_line
where imp_ref = :imp_ref and coalesce(value_id1, 0) < 0
;

------------------------------------------------------------------------------------------------------------------------
/*
Update incitranslaion's value for existing incitranslations.
*/
-- US translation.
update anx.incitranslation
set tuv_seg      = trim(value12),
    tuv_charonly = upper(trim(translate(value12, '1234567890', '')))
from import.import_line
where incitranslation_id = value_id2
  and imp_ref = :imp_ref
  and value_id2 > 0
;

-- EU translation.
update anx.incitranslation
set tuv_seg      = trim(value13),
    tuv_charonly = upper(trim(translate(value13, '1234567890', '')))
from import.import_line
where incitranslation_id = value_id3
  and imp_ref = :imp_ref
  and value_id3 > 0
;

-- CA translation (CA translation is always the same with inciname).
update anx.incitranslation
set tuv_seg      = trim(value11),
    tuv_charonly = upper(trim(translate(value11, '1234567890', '')))
from import.import_line
where incitranslation_id = value_id4
  and imp_ref = :imp_ref
;

------------------------------------------------------------------------------------------------------------------------
/*
Insert translations and link them.
*/

/* Insert US translation. */
insert into anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country, co)
select abs(value_id2), value12, upper(trim(translate(value12, '1234567890', ''))), 'US', ''
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id2, 0) < 0
;

/* Insert EU translation. */
insert into anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country, co)
select abs(value_id3), value13, upper(trim(translate(value13, '1234567890', ''))), 'EU', ''
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id3, 0) < 0
;

/* Insert EU translation. */
insert into anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country, co)
select abs(value_id4), value11, upper(trim(translate(value11, '1234567890', ''))), 'CA', ''
from import.import_line
where imp_ref = :imp_ref
  and coalesce(value_id4, 0) < 0
;

------------------------------------------------------------------------------------------------------------------------

/* link the new translations. */
insert into anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('anx.seq_entr_inci_incitranslation_id'), abs(value_id1), abs(value_id2), 'TRANSLATION', ''
from import.import_line
where imp_ref = :imp_ref and
      value_id1 is not null and
      value_id2 is not null
group by value_id1, value_id2
;

/* link the new translations. */
insert into anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('anx.seq_entr_inci_incitranslation_id'), abs(value_id1), abs(value_id3), 'TRANSLATION', ''
from import.import_line
where imp_ref = :imp_ref
  and value_id1 is not null
  and value_id3 is not null
group by value_id1, value_id3
;

/* link the new translations. */
insert into anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
select nextval('anx.seq_entr_inci_incitranslation_id'), abs(value_id1), abs(value_id4), 'TRANSLATION', ''
from import.import_line
where imp_ref = :imp_ref
  and value_id1 is not null
  and value_id4 is not null
group by value_id1, value_id4
;
/*
update import.import_line
set   value10 = null
where value10 = 'NaN' and imp_ref = :imp_ref;
 */

------------------------------------------------------------------------------------------------------------------------

/* extract mismatched inci's */
select value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11
from import.import_line
where imp_ref = :imp_ref
  and value_id1 is null;


