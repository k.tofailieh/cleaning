
--------------------------------------------------------------------------------------------
/*reindexing aqua-inci relations into water-inci 45766--> 69688 */

/* 
 * Aqua-inci id = 45766
 * Water-inci id = 69688
 * */

-- as src:
UPDATE anx.entr_inci_app SET entr_src_id = 69688 WHERE entr_src_id= 45766;
UPDATE anx.entr_inci_endpt SET entr_src_id = 69688 WHERE entr_src_id= 45766;
UPDATE anx.entr_inci_incitranslation SET entr_src_id = 69688 WHERE entr_src_id= 45766;
UPDATE anx.entr_inci_study SET entr_src_id = 69688 WHERE entr_src_id= 45766;

-- as destination:
UPDATE anx.entr_fiche_inci SET entr_dst_id = 69688 WHERE entr_dst_id= 45766;
UPDATE anx.entr_regentry_inci SET entr_dst_id = 69688 WHERE entr_dst_id= 45766;
UPDATE anx.entr_subset_inci SET entr_dst_id = 69688 WHERE entr_dst_id= 45766;
UPDATE anx.entr_substances_inci SET entr_dst_id = 69688 WHERE entr_dst_id= 45766;
UPDATE anx.entr_study_inci SET entr_dst_id = 69688 WHERE entr_dst_id= 45766;

-----------------------------------------------------------------------------------------------------
-- delete aqua-inci
DELETE FROM anx.inci WHERE inci_id = 45766;


