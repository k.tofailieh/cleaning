 /*
  * EQL from add substance in Cosmetic-Factory.
  * Edition:
  * -add incitranslations in one line.
  * - splitting them by ', '
  * */
 
select
		i.inci_id,
		i.inciname ,
		i.tp,
		substances.sub_id,
		substances.naml as naml,
		substances.cas as cas,
		substances.ec as ec,
		substances.idx as idx,
		classification.entrSubstancesPlcData as entr_substances_plc,
(
		 CASE WHEN COALESCE(us.tuv_seg, '') != '' THEN (COALESCE(us.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END ||
         CASE WHEN COALESCE(ca.tuv_seg,'')  != '' THEN (COALESCE(ca.tuv_seg,'')  || ', ')::TEXT ELSE ''::TEXT END ||
         CASE WHEN COALESCE(eu.tuv_seg, '') != '' THEN (COALESCE(eu.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END ||
         CASE WHEN COALESCE(cn.tuv_seg, '') != '' THEN (COALESCE(cn.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END ||
        COALESCE(jp.tuv_seg, '') || '.'
) AS tuv_seg
		from anx.inci i 
/* get all translations in one cell splitted by ',' */
LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'US') us ON us.entr_src_id = i.inci_id 
LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'CA') ca ON ca.entr_src_id = i.inci_id 
LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'EU') eu ON eu.entr_src_id = i.inci_id 
LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'CN') cn ON cn.entr_src_id = i.inci_id 
LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'JP') jp ON jp.entr_src_id = i.inci_id 
	--
left join anx.entr_substances_inci esi on esi.entr_dst_id =i.inci_id 
left join substances on	esi.entr_src_id = substances.sub_id
----
		left outer join lateral(
			select esp.entr_src_id,
			concat(
				'[',
				string_agg(concat(
				'{',
					'"entrId":',esp.entr_id,',',
					'"entrTp":"',esp.entr_tp,'",',
					'"dst":{',
						'"theRawId":',plc.plc_id,',',
						'"allPlcsys":',COALESCE(allplcsys.entrPlcPlcsysData, '[]'),
					'}',
				'}'
				),','),
				']'
			) as entrSubstancesPlcData
			from entr_substances_plc as esp
			left outer join plc on plc.plc_id = esp.entr_dst_id
			--
			left outer join lateral(
				select allplcsys.entr_src_id,
				concat(
					'[',
					string_agg(concat(
					'{',
						'"entrId":',allplcsys.entr_id,',',
						'"entrTp":"',allplcsys.entr_tp,'",',
						'"dst":{',
							'"theRawId":',plcsys_src.plcsys_id,',',
							'"plcsysKe":"',plcsys_src.plcsys_ke,'",',
							'"tp":"',plcsys_src.tp,'",',
							'"allPlcsyses":',COALESCE(allplcsyses.entrPlcsysPlcsysData, '[]'),
						'}',
					'}'
					),','),
					']'
				) as entrPlcPlcsysData
				from entr_plc_plcsys as allplcsys
				left outer join plcsys as plcsys_src on plcsys_src.plcsys_id = allplcsys.entr_dst_id
				left outer join lateral(
					select 
					allplcsyses.entr_src_id,
					concat(
						'[',
						string_agg(concat(
						'{',
							'"entrId":',allplcsyses.entr_id,',',
							'"entrTp":"',allplcsyses.entr_tp,'",',
							'"dst":{',
								'"theRawId":',plcsys_dst.plcsys_id,',',
								'"plcsysKe":"',plcsys_dst.plcsys_ke,'",',
								'"tp":"',plcsys_dst.tp,'"',
							'}',
						'}'
						),','),
						']'
					) as entrPlcsysPlcsysData
					from entr_plcsys_plcsys as allplcsyses
					inner join plcsys as plcsys_dst on plcsys_dst.plcsys_id  = allplcsyses.entr_dst_id
					where allplcsyses.entr_src_id = plcsys_src.plcsys_id
					group by allplcsyses.entr_src_id
				) as allplcsyses on allplcsyses.entr_src_id = plcsys_src.plcsys_id
				where allplcsys.entr_src_id = plc.plc_id 
--				and allplcsys.entr_tp in ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
--				and plcsys_src.tp in ('CATEGORIZE','RISK')
				group by allplcsys.entr_src_id
			) as allplcsys on allplcsys.entr_src_id = plc_id	
			where esp.entr_src_id = substances.sub_id
--			and esp.entr_tp in('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
			and plc.ns in ('*', 'EcoMundo')				
			group by esp.entr_src_id
		) as classification on classification.entr_src_id = substances.sub_id
		WHERE i.anx_st = 'VALID' and i.inci_id = 69688;