    SELECT anx.inci.*,
           		concat(
		 CASE WHEN COALESCE(us.tuv_seg, '') != '' THEN (COALESCE(us.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END,
         CASE WHEN COALESCE(ca.tuv_seg,'')  != '' THEN (COALESCE(ca.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END,
         CASE WHEN COALESCE(eu.tuv_seg, '') != '' THEN (COALESCE(eu.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END,
         CASE WHEN COALESCE(cn.tuv_seg, '') != '' THEN (COALESCE(cn.tuv_seg, '') || ', ')::TEXT ELSE ''::TEXT END,
        COALESCE(jp.tuv_seg, '') || '.'
) AS tuv_seg
    FROM anx.inci
    LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'US') us ON us.entr_src_id = inci_id
    LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'CA') ca ON ca.entr_src_id = inci_id
    LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'EU') eu ON eu.entr_src_id = inci_id
    LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'CN') cn ON cn.entr_src_id = inci_id
    LEFT OUTER JOIN (SELECT * FROM anx.entr_inci_incitranslation INNER JOIN anx.incitranslation ON incitranslation_id = entr_dst_id AND country = 'JP') jp ON jp.entr_src_id = inci_id
    WHERE inci_id  = 69688
    ORDER BY inciname;



SELECT string_agg( COALESCE(NULL, '') || 'hi' || 'nnn'  , ',');


select column_name, table_name from information_schema.columns where column_name LIKE  'nom%'
