
/* 
 * Delete INCI 45766 Aqua and replace it by INCI Water 69688 
 * 
 * old_ids is the Aqua id
 * new_ids is the Water id.
 */

SELECT public.reindex_delete_entity('inci', ARRAY[45766], ARRAY[69688]);


/* reindex Onsen-Sui Substance (2631640) into Water Substance (28900). */
SELECT public.reindex_delete_entity('substances', ARRAY[2631640], ARRAY[28900]);


